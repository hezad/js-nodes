# JSNodes

Just having some fun with "circuit nodes" and modular stuff. 

* No, this has nothing to do with Node.js
* Yes, it's confusing




## Important when adding features

The `app` object located in [`app.js`](app.js) has a `FILE_VERSION` key (at the very top of the file). 

Incrementing this number means JSON files exported with this version are not compatible with previous versions of JSNodes anymore.

It's important to bump this number when you implement new features that can be saved and loaded if these features are mandatory for the app to work.



## Documentation

Yeah, no. I don't even know why or how you would stumble upon this repository. Feel free to install and try it if you want but don't expect any documentation anytime soon.



## License

You shoudn't reuse this code since it's really just some stuff pasted altogether without any kind of prior thinking.

Anyway, if you find some piece(s) of code you'd like to use, consider this repository licensed under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.

That means do whatever you want with it ; but you have to mention both the name and the URL of this repository somewhere in your codebase.



