/* **************** */
/*  Error Handlers  */
/* **************** */

var app_error = {

	fatal: function(message) {
		if( message !== undefined ) {
			console.error(message + "\n");
		}
		this_uncaught_reference_error_is_only_here_to_stop_this_script_execution();
	},

	warn: function(message) {
		if( message !== undefined ) {
			console.warn(message + "\n");
		}
	},

	info: function(message) {
		if( message !== undefined ) {
			console.info(message + "\n");
		}
	},
}


/* **************** */
/*    UI Handlers   */
/* **************** */
var app_modal = {
	was_initiated: false,
	registered_listeners: {
		'hez-helper-list-item': null
	},

	/* Core methods */
	init: function() {
		/* Close icon handler */
		$('body').on('click', '.hez-helper-modal .hez-helper-close', function(e) {
			e.preventDefault();
			app_modal._close_modal();
		});

		/* Close on overlay click handler */
		$('body').on('click', '.hez-helper-overlay', function(e) {
			e.preventDefault();
			app_modal._close_modal();
		});

		/* Click on list select item */
		$('body').on('click', ".hez-helper-modal .hez-helper-list-item", function(e) {
			e.preventDefault();
			var cb = app_modal.registered_listeners['hez-helper-list-item'];
			if( cb && typeof cb === "function" ) {
				var item_id = $(this).attr('data-id');
				cb(item_id);
				app_modal.registered_listeners['hez-helper-list-item'] = null;
				app_modal._close_modal();
			}
			return false;
		})

		/* Set initiated flag to true */
		app_modal.was_initiated = true;
	},

	_show: function(title, item_class, html) {
		if( ! app_modal.was_initiated ) {
			app_modal.init();
		}

		var modal_html = '<div class="hez-helper-overlay"></div>';
		modal_html += '<div class="hez-helper-modal '+item_class+'">';
		modal_html += 	'<div class="hez-helper-modal-header">'+title+'<a href="#" class="hez-helper-close">⨉</a></div>';
		modal_html += 	'<div class="hez-helper-modal-content">';
		modal_html += 		html;
		modal_html += 	'</div>';
		modal_html += '</div>';

		var $modal = $(modal_html);
		$modal.appendTo('body');
	},

	_remove_listeners: function() {
		for(var modal_type in app_modal.registered_listeners) {
			app_modal.registered_listeners[modal_type] = null;
		}
	},

	_close_modal: function() {
		$('.hez-helper-modal').remove();
		$('.hez-helper-overlay').remove();

		app_modal._remove_listeners();
	},


	/* Modals */
	alert: function(message) {
		alert(message);
	},

	success: function(message) {
		alert(message);
	},

	confirm: function(message) {
		return confirm(message);
	},

	prompt: function(message, default_value) {
		return window.prompt(message, default_value);
	},

	list_select: function(title, items, on_select) {
		var modal_html = '';
		modal_html += '<ul class="hez-helper-list-select-ul">';
		for(var item_id in items) {
			var item_title = items[item_id];
			modal_html += '<li><a class="hez-helper-list-item" href="#" data-id="'+item_id+'">'+item_title+'</span></li>';
		}
		modal_html += '</ul>';
		
		if( on_select && typeof on_select === "function" ) {
			app_modal.registered_listeners['hez-helper-list-item'] = on_select;
		}
		app_modal._show(title, 'hez-helper-list-select', modal_html);
	}
}




/* ***************** */
/*    File helpers   */
/* ***************** */

var download_file = function(text, name, type) {
	if( ! type ) {
		type = "text/plain";
	}

    var a = document.createElement("a");
    var file = new Blob([text], {type: type});
    a.href = URL.createObjectURL(file);
    a.download = name;
    a.click();
}

var load_json_file = function(on_load_callback) {
	if( (! on_load_callback) || typeof on_load_callback !== "function" ) {
		app_modal.alert("Error when trying to load a file.\n\n Check console for more info.");
		app_error.warn("load_json_file() expects a function as it's first argument. Aborting loading.");
		return;
	}

	if (window.File && window.FileReader && window.FileList && window.Blob) {
		(function(user_callback) {
			var file_input = document.createElement("input");
			file_input.type = "file";
			file_input.addEventListener('change', function(e) {
				var files = e.target.files;
				var file = files[0];

				if( ! file.type.match("json") ) {
					app_modal.alert("Can't load " + file.name + ".\n\n Check console for more info.");
					app_error.warn(file.name + " is not a JSON file. Aborting loading.");
					return;
				}

				var reader = new FileReader();
				reader.onload = (function(the_file, the_user_callback) {
					return function(e) {
						var json_text_data = e.target.result;

						try {
							var json_data = JSON.parse(json_text_data);
							the_user_callback(json_data, the_file);
						} catch (ex) {
							app_modal.alert("Can't parse " + the_file.name + ".\n\n Check console for more info.");
							app_error.warn("Can't parse file "+the_file.name+".\n\nError when parsing JSON:\n" + ex + "\n\nAborting loading.");
							return;
						}
					};
				})(file, user_callback);

				reader.readAsText(file);

			}, false);

			file_input.click();
		}) (on_load_callback);
	} else {
		app_modal.alert("Can't load local files.\n\n Check console for more info.");
		app_error.warn('The File APIs are not fully supported in this browser. Can\'t load files from system.');
		return;
	}
}



/* **************** */
/*  String helpers  */
/* **************** */

var string_to_nice_filename = function(input) {
	return input.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}





/* **************** */
/*  Events helpers  */
/* **************** */

var on_after_window_resize = function(listener_callback) {
	if( window.resized_listener_index === undefined ) {
		window.resized_listener_index = 0;
		window.resized_listener_timeout_handles = []; 
	} else {
		window.resized_listener_index += 1;
	}

	( function(resized_listener_index, listener_callback) {
		$(window).resize(function(e) {
		    clearTimeout(window.resized_listener_timeout_handles[resized_listener_index]);
		    window.resized_listener_timeout_handles[resized_listener_index] = setTimeout(function(){
		        listener_callback(e);
		    }, 250);
		});
	}) (window.resized_listener_index, listener_callback);
}

