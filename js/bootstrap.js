
/* prevents requireJS to consider packages named <something>.js to be considered like "non-modules" */
require.jsExtRegExp = /^\/|:|\?$/;


/* requireJS configuration */
require.config({
	baseUrl: "js",
	paths: {
		'pixi.js': 'vendor/pixi.min',
	},
	shim: {
		'models/Circuit': ['models/Node']
	},
});


/* Dependencies loading */
require([
	'vendor/dat.gui.min',
	'pixi.js', 
	'vendor/jquery.3.2.1.min',  
	'helpers',
	'models/Node',
	'models/Circuit',
	'models/App'
], function(dat, PIXI) {
	window.dat = dat;

	if( window.on_ready ) {
		$( function() {
			window.on_ready();
		});
	}
});

