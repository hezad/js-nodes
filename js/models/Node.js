/**
 * Node Model
 *
 * Core / Default Nodes are registered at the end of this file
 */


var NODE_CONTAINER_BORDER_SIZE = 4;
var NODE_SETTINGS_VERTICAL_MARGIN = 4;
var NODE_TITLE_HEADER_HEIGHT = 20;
var NODE_CONNECTORS_SIZE = 10;
var NODE_CONNECTORS_OFFSET = 5;
var NODE_IMAGE_SIZE = 0.4;
var NODE_DEFAULT_NUM_INPUTS = 1;
var NODE_DEFAULT_NUM_OUTPUTS = 1;
var NODE_CONNECTORS_LABEL_COLOR = 0xdddddd;
var NODE_CONNECTORS_LABEL_OFFSET = 10;
var NODE_GFX_AXIS_COLOR = 0xbbbbbb;
var NODE_GFX_POINT_COLOR = 0xff0000;
var NODE_DEFAULT_NUM_OUTPUT_DECIMAL_PLACES = 2;


/*********************************/
/*********************************/
/**    NUM OUTPUT FORMATTER     **/
/*********************************/
/*********************************/

var convert_to_output_mode = function(input, as_string) {
	var output_mode = Circuit.settings.output_mode;
	
	if( output_mode != "decimal" ) {
		input = Math.round(input);
	}
	
	input = parseFloat(Math.round(input * 100) / 100).toFixed(NODE_DEFAULT_NUM_OUTPUT_DECIMAL_PLACES);
	
	return input.toString(Node.all_output_modes[output_mode]);
}



/*********************************/
/*********************************/
/**         NODE MODEL          **/
/*********************************/
/*********************************/


var Node = {
	num_instanciated_nodes: 0,

	all_output_modes: {
		"binary": 2,
		"decimal": 10,
		"hexadecimal": 16,
	},

	registered_nodes: {
		DefaultNode: function() {
			var this_node = this;

			this.num_inputs = NODE_DEFAULT_NUM_INPUTS;
			this.num_outputs = NODE_DEFAULT_NUM_OUTPUTS;
			this.variable_data = {};

			this.inputs = [];
			this.outputs = [];

			this.is_node_object = true;
			this.nice_name = 'Default';

			this.is_editable = false;
			this.on_editor_show = null;

			this.is_moving = false;

			this.image_path = null;
			this.image_sprite = null;
			this.image_is_loaded = false;

			this.node_type = "DefaultNode";
			this.node_identifier = null;
			this.gfx_output_type = null;

			this.gfx = null;
			this.gfx_rectangle = null;
			this.gfx_title_header = null;
			this.gfx_title = null;
			this.gfx_output = null;

			this.__gfx_settings_to_text_output = {};



			this.gfx_input_connectors = [];
			this.gfx_output_connectors = [];

			this.gfx_input_labels = [];
			this.gfx_output_labels = [];

			this.gfx_input_connection_lines = {};
			this.gfx_output_connection_lines = {};

			this.labels = null;

			this.gfx_get_output_connector_position = function(index, center) {
				var w = this.gfx_rectangle.width;
				var h = this.gfx_rectangle.height
				var height_per_output = h / this.num_outputs;
				return {
					x: NODE_CONNECTORS_OFFSET + w - NODE_CONNECTORS_SIZE * 0.5 + (center ? NODE_CONNECTORS_SIZE * 0.5 - 1: 0), 
					y: index * height_per_output + height_per_output * 0.5 - NODE_CONNECTORS_SIZE * 0.5 + (center ? NODE_CONNECTORS_SIZE * 0.5 -1 : 0),
				};
			}

			this.gfx_get_input_connector_position = function(index, center) {
				var w = this.gfx_rectangle.width;
				var h = this.gfx_rectangle.height
				var height_per_input = h / this.num_inputs;
				return {
					x: - NODE_CONNECTORS_OFFSET - NODE_CONNECTORS_SIZE * 0.5 + (center ? NODE_CONNECTORS_SIZE * 0.5 -1 : 0) - 4, 
					y: index * height_per_input + height_per_input * 0.5 - NODE_CONNECTORS_SIZE * 0.5 + (center ? NODE_CONNECTORS_SIZE * 0.5 -1 : 0),
				};
			}

			this.get_text_output_string = function(value, output_mode) {
				var input_value = (value !== undefined) ? value : this.value;

				if( input_value === null || input_value === undefined ) {
					return "0";
				}

				var output_text = convert_to_output_mode(input_value);
	
				return output_text;
			}

			this.update_text_output = function() {
				var x_offset = NODE_CONTAINER_BORDER_SIZE;
				var y_offset = NODE_TITLE_HEADER_HEIGHT + NODE_CONTAINER_BORDER_SIZE * (this.is_editable ? 2 : 1);

				if( this.gfx_output_type == "settings" ) {
					var settings_default_values = this.settings_default_value;
					for(var setting_key in settings_default_values) {
						var output_text = this.get_text_output_string(this.variable_data[setting_key]);
						var gfx_output_text = this.__gfx_settings_to_text_output[setting_key];

						gfx_output_text.setText(output_text);

						gfx_output_text.position.set(
							this.gfx_output.width * 0.5 - gfx_output_text.width * 0.5,
							gfx_output_text.position.y
						);
					}
				} else if (this.gfx_output_type == "text") {
					var output_text = this.get_text_output_string();
				
					this.gfx_output_text.setText(output_text);

					this.gfx_output_text.position.set(
						x_offset + this.gfx_output.width * 0.5 - this.gfx_output_text.width * 0.5,
						y_offset + this.gfx_output.height * 0.5 - this.gfx_output_text.height * 0.5,
					);

					return;
				}
				
			}

			this.launch_editor = function() {
				var that_node = this;

				var save_value_callback = (function(that_node) {
					return function(user_value, setting_key, value_is_a_string) {
						that_node.variable_data[setting_key] = (value_is_a_string ? user_value : Number(user_value));
						that_node.update_text_output();
					}
				}) (that_node);

				this.on_editor_show.call(this, save_value_callback);
			}

			this.gfx_update_position = function(new_position) {
				this.gfx.position.x = new_position.x;
			    this.gfx.position.y = new_position.y;
			}

			this.gfx_update_frame = function() {
				var gfx_input_lines = this.gfx_input_connection_lines; 
				var gfx_input_labels = this.gfx_input_labels; 

			    var gfx_output_lines = this.gfx_output_connection_lines; 
			    var gfx_output_labels = this.gfx_output_labels; 

			    for(var i in gfx_input_lines) {
			    	Node.update_connection_line_on_position_change(gfx_input_lines[i], gfx_input_labels[i], "input");
			    }

			    for(var i in gfx_output_lines) {
			    	Node.update_connection_line_on_position_change(gfx_output_lines[i], gfx_output_labels[i], "output");
			    }

			    if( this.on_each_frame ) {
			    	this.on_each_frame.call(this);
			    }
			}

			this.make_on_connector_drag_start_listener = function(inout_type, connector_gfx) {
				return (function(inout_type, connector_gfx) {
					return function(event) {
						Circuit.connecting_interaction.data.from = {
							inout_type: inout_type,
							connector_gfx: connector_gfx,
						};
						
						Circuit.connecting_interaction.is_active = true;

						Circuit.connecting_interaction.temp_connection_line_gfx = new PIXI.Graphics();
						Circuit.gfx_engine.stage.addChild(Circuit.connecting_interaction.temp_connection_line_gfx);
					}
				}) (inout_type, connector_gfx);
			}

			this.make_on_connector_drag_end_listener = function(inout_type, connector_gfx) {
				return (function(inout_type, connector_gfx) {
					return function(event) {
						var from_inout_type = Circuit.connecting_interaction.data.from.inout_type;
						var from_connector_gfx = Circuit.connecting_interaction.data.from.connector_gfx;
						var from_identifier = from_connector_gfx.parentNode.node_identifier + ":" + from_connector_gfx.node_connector_index;

						var to_inout_type = connector_gfx.node_connector_type;
						var to_connector_gfx = connector_gfx;
						var to_identifier = connector_gfx.parentNode.node_identifier + ":" + connector_gfx.node_connector_index;

						if( Circuit.connecting_interaction.temp_connection_line_gfx ) {
							Circuit.gfx_engine.stage.removeChild(Circuit.connecting_interaction.temp_connection_line_gfx);
							Circuit.connecting_interaction.temp_connection_line_gfx.destroy(true);
							Circuit.connecting_interaction.temp_connection_line_gfx = null;
						}

						if( Circuit.is_connection_valid(from_inout_type, from_connector_gfx, to_inout_type, to_connector_gfx) ) {
							if( from_inout_type == "output" ) {
								Circuit.connect_nodes({
									from: { node: from_connector_gfx.parentNode, output: from_connector_gfx.node_connector_index },
									to: { node: to_connector_gfx.parentNode, input: to_connector_gfx.node_connector_index }
								});
							} else {
								Circuit.connect_nodes({
									to: { node: from_connector_gfx.parentNode, output: from_connector_gfx.node_connector_index },
									from: { node: to_connector_gfx.parentNode, input: to_connector_gfx.node_connector_index }
								});
							}
							
						}
						
						Circuit.connecting_interaction.is_active = false;
					};
				}) (inout_type, connector_gfx);
			}

			this.make_on_connector_drag_move_listener = function() {
				return function(event) {
					if( Circuit.connecting_interaction.is_active ) {
						var from_inout_type = Circuit.connecting_interaction.data.from.inout_type;
						var from_connector_gfx = Circuit.connecting_interaction.data.from.connector_gfx;

						if( from_inout_type == "input" ) {
							var from_position = from_connector_gfx.parentNode.gfx_get_input_connector_position(from_connector_gfx.node_connector_index, true);
						} else {
							var from_position = from_connector_gfx.parentNode.gfx_get_output_connector_position(from_connector_gfx.node_connector_index, true);
						}
						
						from_position.x += from_connector_gfx.parentNode.gfx.position.x;
						from_position.y += from_connector_gfx.parentNode.gfx.position.y;

						var to_position = Circuit.gfx_engine.renderer.plugins.interaction.mouse.global;
						
						Circuit.connecting_interaction.temp_connection_line_gfx.clear();
						Circuit.connecting_interaction.temp_connection_line_gfx.beginFill(0xffffff)
							.lineStyle(2, 0xffffff, 0.5)
							.moveTo(from_position.x, from_position.y)
					        .lineTo(to_position.x, to_position.y)
					        .endFill();
					}
				}
			}

			this.on_rectangle_drag_start = function(event) {
				var that = this_node.gfx;

			    that.data = event.data;
			    
			    that.drag_offset = {
			    	x: event.data.global.x - that.position.x,
			    	y: event.data.global.y - that.position.y,
			    }

			    that.dragging = true;
			}

			this.on_rectangle_drag_end = function(event) {
				var that = this_node.gfx;

				that.dragging = false;
				that.alpha = 1;

				if( ! that.is_moving ) {
					// Not a dragend event but a mouseup (considered as a "click") event
					if( this_node.is_editable ) {
						this_node.launch_editor();
					}
				}

				// Actual dragend event
			    that.is_moving = false;
			    that.data = null;
			}

			this.on_rectangle_drag_move = function() {
				var that = this_node.gfx;

			    if(! that.dragging) {
			    	return;
			    }
				
		    	that.is_moving = true;
		    	that.alpha = 0.5;

		        var new_position = that.data.getLocalPosition(that.parent);
		        new_position.x -= that.drag_offset.x;
		        new_position.y -= that.drag_offset.y;

		        this_node.gfx_update_position(new_position);
			}

			this.gfx_init = function(stage, x, y) {
				
				var w = 100;
				var h = 60;

				var rect_border_color = this.is_editable ? 0xc0cffe : 0xaaaaaa;
				var rect_bg_color	  = 0xeeeeee;
				var title_bg_color	  = this.is_editable ? 0xc0cffe : 0xffffff;
				var value_txt_color	  = this.is_editable ? 0x30364d : 0x404040;
				var setting_bg_color  = 0xd2d6e5;
				var title_setting_color	= 0x404655;
				var input_to_border_spc = this.is_editable ? 4 : 0;

				var node_settings_out_values = null;

				if( this.gfx_output_type == "settings" ) {
					if( this.settings_default_value && typeof this.settings_default_value === "object" ) {
						node_settings_out_values = this.settings_default_value;
					}

					if( node_settings_out_values === null || typeof node_settings_out_values !== "object" ) {
						app_error.fatal('[Error when initiating node "'+this.node_type+'"] Nodes registered with gfx_input_mode == "settings" expect a key "settings_default_value" (that is an object)');
						return;
					}
				}

				if( this.width ) { w = this.width; } 
				if( this.height ) { h = this.height; } 

				/* Node GFX Container */
				this.gfx = new PIXI.Container();

				var x0 = NODE_CONTAINER_BORDER_SIZE + input_to_border_spc;
				var y0 = NODE_TITLE_HEADER_HEIGHT + NODE_CONTAINER_BORDER_SIZE + input_to_border_spc;
				var x1 = w - NODE_CONTAINER_BORDER_SIZE * 2 - input_to_border_spc * 2;
				var y1 = h - NODE_CONTAINER_BORDER_SIZE * 2 - input_to_border_spc * 2;

				var output_gfx_width = x1;
				var output_gfx_height = y1;

				var gfx_to_output = null;

				switch( this.gfx_output_type ) {
					case "text":
						this.gfx_output_text = new PIXI.Text(
							this.value,
							{
								fontFamily: "Helvetica, Arial, Verdana, Sans", 
								fontSize: 14, 
								fontWeight: "400",
								fill: value_txt_color
							}
						);
					
						this.gfx_output_text.position.set(
							output_gfx_width * 0.5 - this.gfx_output_text.width * 0.5,
							output_gfx_height * 0.5 - this.gfx_output_text.height * 0.5,
						);
						
						gfx_to_output = this.gfx_output_text;
						
						break;

					case "settings":
						
						var setting_index = 0;
						var gfx_settings_output = new PIXI.Graphics();
						var this_output_gfx_height = 0;

						for(var setting_title in node_settings_out_values) {
							var setting_value = node_settings_out_values[setting_title];

							var setting_gfx_output_title = new PIXI.Text(
								setting_title,
								{
									fontFamily: "Helvetica, Arial, Verdana, Sans", 
									fontSize: 12, 
									fontWeight: "300",
									fill: title_setting_color
								}
							);

							var setting_gfx_output_text = new PIXI.Text(
								setting_value,
								{
									fontFamily: "Helvetica, Arial, Verdana, Sans", 
									fontSize: 14, 
									fontWeight: "400",
									fill: value_txt_color
								}
							);

							var new_y0 = NODE_SETTINGS_VERTICAL_MARGIN * 2 + (2 * NODE_SETTINGS_VERTICAL_MARGIN + setting_gfx_output_title.height + setting_gfx_output_text.height * 1.4) * setting_index;

							var setting_gfx_output_rect = new PIXI.Graphics();
							setting_gfx_output_rect.lineStyle(0, 0, 1);
							setting_gfx_output_rect.beginFill(setting_bg_color);
							setting_gfx_output_rect.drawRect(x0, new_y0, x1, setting_gfx_output_text.height * 1.5);
							setting_gfx_output_rect.endFill();

							setting_gfx_output_title.position.set(
								x0,
								new_y0,
							);

							setting_gfx_output_rect.position.set(
								0,
								setting_gfx_output_text.height
							);

							setting_gfx_output_text.position.set(
								setting_gfx_output_rect.width * 0.5 - setting_gfx_output_text.width * 0.5,
								new_y0 + setting_gfx_output_rect.height * 0.5 - setting_gfx_output_text.height * 0.5
							);

							this_output_gfx_height += setting_gfx_output_title.height + setting_gfx_output_rect.height;
							this.__gfx_settings_to_text_output[setting_title] = setting_gfx_output_text;

							setting_index += 1;

							setting_gfx_output_rect.addChild(setting_gfx_output_text);

							gfx_settings_output.addChild(setting_gfx_output_title);
							gfx_settings_output.addChild(setting_gfx_output_rect);
						}

						h = NODE_TITLE_HEADER_HEIGHT + NODE_SETTINGS_VERTICAL_MARGIN * 2 + this_output_gfx_height;
						gfx_to_output = gfx_settings_output;

						break;

					default:
						if( this.on_gfx_init ) {
							gfx_to_output = this.on_gfx_init(this.width, this.height);
						}
						break;
				}

				this.gfx_rectangle = new PIXI.Graphics();
				this.gfx_rectangle.lineStyle(NODE_CONTAINER_BORDER_SIZE, rect_border_color, 1);
				this.gfx_rectangle.beginFill(rect_bg_color);
				this.gfx_rectangle.drawRect(0, 0, w, h + NODE_TITLE_HEADER_HEIGHT);
				this.gfx_rectangle.endFill();
				this.gfx_rectangle.interactive = true;
				this.gfx_rectangle.buttonMode = true;

				this.gfx_output = new PIXI.Graphics();
				this.gfx_output.lineStyle(0, 0, 1);
				this.gfx_output.beginFill(0xeeeeee);
				this.gfx_output.drawRect(x0, y0, output_gfx_width, output_gfx_height);
				this.gfx_output.endFill();

				if( gfx_to_output ) {
					gfx_to_output.position.set(gfx_to_output.position.x, gfx_to_output.position.y + NODE_TITLE_HEADER_HEIGHT);
					this.gfx_output.addChild(gfx_to_output);
				}
				

				/* Node Title GFX */
				/* Header GFX Container */
				this.gfx_title_header = new PIXI.Graphics();
				this.gfx_title_header.beginFill(title_bg_color);
				this.gfx_title_header.drawRect(
					NODE_CONTAINER_BORDER_SIZE - 2, 
					NODE_CONTAINER_BORDER_SIZE - 2, 
					w - NODE_CONTAINER_BORDER_SIZE * 2 + 4, 
					NODE_TITLE_HEADER_HEIGHT
				);
				this.gfx_title_header.endFill();

				/* Text GFX */
				this.gfx_title = new PIXI.Text(
					this.nice_name,
					{
						fontFamily: "Helvetica, Arial, Verdana, Sans", 
						fontSize: 13, 
						fill: "#404040",
					}
				);
				
				this.gfx_title.position.set(
					this.gfx_title_header.width * 0.5 - this.gfx_title.width * 0.5, 
					NODE_TITLE_HEADER_HEIGHT * 0.5 - this.gfx_title.height * 0.5 + 2, 
				);

				/* Output connector GFX */
				if( this.num_outputs ) {
					for(var ni = 0; ni < this.num_outputs; ni ++) {
						var connector_position = this.gfx_get_output_connector_position(ni);
						var this_gfx_output_connector = new PIXI.Graphics();
						this_gfx_output_connector.lineStyle(1, 0x6666ee, 1);
						this_gfx_output_connector.beginFill(0xaaeeee);
						this_gfx_output_connector.drawRect(
							connector_position.x, 
							connector_position.y,
							NODE_CONNECTORS_SIZE, 
							NODE_CONNECTORS_SIZE
						);
						this_gfx_output_connector.node_connector_index = ni;
						this_gfx_output_connector.node_connector_type = "output";
						this_gfx_output_connector.endFill();

						var label_text = 'out#'+ni;
						if( this.num_outputs == 1 ) {
							label_text = 'out';
						}
						if( this.labels && this.labels.out ) {
							label_text = this.labels.out[ni];
						}

						var this_gfx_output_label = new PIXI.Text(label_text, {
							fill: NODE_CONNECTORS_LABEL_COLOR,
							fontSize: 12,
						});

						this.gfx_output_connectors.push(this_gfx_output_connector);
						this.gfx_output_labels.push(this_gfx_output_label);

						this_gfx_output_connector.interactive = true;
						this_gfx_output_connector.buttonMode = true;
						this_gfx_output_connector.parentNode = this;

						this.gfx.addChild(this_gfx_output_connector);
						this.gfx.addChild(this_gfx_output_label);

						/* Node Connector Interactive listeners bindings */
						this_gfx_output_connector
							// events for drag start
							.on('mousedown', this.make_on_connector_drag_start_listener('output', this_gfx_output_connector))
							.on('touchstart', this.make_on_connector_drag_start_listener('output', this_gfx_output_connector))
							// events for drag end
							.on('mouseup', this.make_on_connector_drag_end_listener('output', this_gfx_output_connector))
							.on('mouseupoutside', this.make_on_connector_drag_end_listener('output', this_gfx_output_connector))
							.on('touchend', this.make_on_connector_drag_end_listener('output', this_gfx_output_connector))
							.on('touchendoutside', this.make_on_connector_drag_end_listener('output', this_gfx_output_connector))
							// events for drag move
							.on('mousemove', this.make_on_connector_drag_move_listener('output', this_gfx_output_connector))
							.on('touchmove', this.make_on_connector_drag_move_listener('output', this_gfx_output_connector));
					}
				}

				/* Input connectors GFX */
				if( this.num_inputs ) {
					for(var ni = 0; ni < this.num_inputs; ni ++) {
						var connector_position = this.gfx_get_input_connector_position(ni);
						var this_gfx_input_connector = new PIXI.Graphics();
						this_gfx_input_connector.lineStyle(1, 0x6666ee, 1);
						this_gfx_input_connector.beginFill(0xaaeeee);
						this_gfx_input_connector.drawRect(
							connector_position.x, 
							connector_position.y,
							NODE_CONNECTORS_SIZE, 
							NODE_CONNECTORS_SIZE
						);
						this_gfx_input_connector.node_connector_index = ni;
						this_gfx_input_connector.node_connector_type = "input";
						this_gfx_input_connector.endFill();

						var label_text = 'in#'+ni;
						if( this.num_inputs == 1 ) {
							label_text = 'in';
						}
						if( this.labels && this.labels.in ) {
							label_text = this.labels.in[ni];
						}
						var this_gfx_input_label = new PIXI.Text(label_text, {
							fill: NODE_CONNECTORS_LABEL_COLOR,
							fontSize: 12,
						});

						this.gfx_input_labels.push(this_gfx_input_label);
						this.gfx_input_connectors.push(this_gfx_input_connector);

						this_gfx_input_connector.interactive = true;
						this_gfx_input_connector.buttonMode = true;
						this_gfx_input_connector.parentNode = this;

						this.gfx.addChild(this_gfx_input_connector);
						this.gfx.addChild(this_gfx_input_label);
				
						/* Node Connector Interactive listeners bindings */
						this_gfx_input_connector
							// events for drag start
							.on('mousedown', this.make_on_connector_drag_start_listener('input', this_gfx_input_connector))
							.on('touchstart', this.make_on_connector_drag_start_listener('input', this_gfx_input_connector))
							// events for drag end
							.on('mouseup', this.make_on_connector_drag_end_listener('input', this_gfx_input_connector))
							.on('mouseupoutside', this.make_on_connector_drag_end_listener('input', this_gfx_input_connector))
							.on('touchend', this.make_on_connector_drag_end_listener('input', this_gfx_input_connector))
							.on('touchendoutside', this.make_on_connector_drag_end_listener('input', this_gfx_input_connector))
							// events for drag move
							.on('mousemove', this.make_on_connector_drag_move_listener('input', this_gfx_input_connector))
							.on('touchmove', this.make_on_connector_drag_move_listener('input', this_gfx_input_connector));
					}
				}

				/* Add GFX items to container */
				this.gfx_title_header.addChild(this.gfx_title);
				this.gfx.addChild(this.gfx_rectangle);
				this.gfx.addChild(this.gfx_title_header);

				this.gfx.addChild(this.gfx_output);
				
				/* Node background image */
				if( this.image_path ) {
					var that = this;
					this.image_sprite = PIXI.Sprite.fromImage(this.image_path);
 					this.image_sprite.texture.baseTexture.on('loaded', function() {
 						that.image_is_loaded = true;

 						// Set sprite size
						var orig_width = that.image_sprite.width;
						var orig_height = that.image_sprite.height;
						var ratio = orig_height / orig_width;

						var new_width = that.gfx_rectangle.width * NODE_IMAGE_SIZE;
						var new_height = new_width * ratio;

						var size_scale = 0.75;

						if( that.image_sprite.height > that.gfx_rectangle.height * 0.8 ) {
							size_scale = 0.5;
						}

						that.image_sprite.width = new_width * size_scale;
						that.image_sprite.height = new_height * size_scale;	

						// center the sprite anchor point
						that.image_sprite.anchor.x = 0.5;
						that.image_sprite.anchor.y = 0.5;

						if( that.is_editable ) {
							size_scale = 0.4;
							that.image_sprite.width = new_width * size_scale;
							that.image_sprite.height = new_height * size_scale;	

							that.image_sprite.position.x = that.image_sprite.width * 0.5 + NODE_CONTAINER_BORDER_SIZE;
							that.image_sprite.position.y = that.image_sprite.height * 0.5 + NODE_CONTAINER_BORDER_SIZE;
						} else {

							that.image_sprite.position.x = that.gfx_rectangle.width * 0.5;
							that.image_sprite.position.y = that.gfx_title_header.height * 0.4 + that.gfx_rectangle.height * 0.5;
						}		
						 
						that.gfx.addChild(that.image_sprite);
 					});
				}

				this.update_text_output();

				/* Add Node GFX to stage */
				this.gfx.position.set(x, y);
				stage.addChild(this.gfx);

				/* Node GFX Container Interactive listeners bindings */
				this.gfx_rectangle
					// events for drag start
					.on('mousedown', this.on_rectangle_drag_start)
					.on('touchstart', this.on_rectangle_drag_start)
					// events for drag end
					.on('mouseup', this.on_rectangle_drag_end)
					.on('mouseupoutside', this.on_rectangle_drag_end)
					.on('touchend', this.on_rectangle_drag_end)
					.on('touchendoutside', this.on_rectangle_drag_end)
					// events for drag move
					.on('mousemove', this.on_rectangle_drag_move)
					.on('touchmove', this.on_rectangle_drag_move);

				// onClick event is called in this.on_rectangle_drag_end()
			}

			this.processor = function() {
				this.value = this.inputs[0].value;
			}

			this.process = function(timer) {
				for(var i = 0; i < this.num_inputs; i++) {
					var input_node = this.inputs[i];
					if( input_node ) {
						input_node.process.call(input_node, timer);
					}
				}
				this.processor.call(this, timer);
			}
		}
	},

	register: function(node_type, settings) {

		if( settings.num_inputs !== undefined && typeof settings.num_inputs != "number" ) {
			app_error.fatal('[Error when registering node "'+node_type+'"] "num_inputs" must be a number (default value: 1)');
			return;
		}

		if( settings.num_outputs !== undefined && typeof settings.num_outputs != "number" ) {
			app_error.fatal('[Error when registering node "'+node_type+'"] "num_outputs" must be a number (default value: 1)');
			return;
		}

		if( settings.processor === undefined ) {
			settings.processor = function() {};
		} else if ( typeof settings.processor != "function" ) {
			app_error.fatal('[Error when registering node "'+node_type+'"] settings object expects a key "processor" with a "function" type');
			return;
		}

		if( settings.gfx_output_type == "settings" ) {
			settings.is_editable = true;
		}

		if( settings.is_editable ) {
			if( settings.on_editor_show === undefined || typeof settings.on_editor_show != "function" ) {
				app_error.fatal('[Error when registering node "'+node_type+'"] editable nodes settings object expects a key "on_editor_show" with a "function" type');
				return;
			}
		}

		var num_outputs = settings.num_outputs !== undefined ? settings.num_outputs : NODE_DEFAULT_NUM_OUTPUTS;
		var num_inputs = settings.num_inputs !== undefined ? settings.num_inputs : NODE_DEFAULT_NUM_INPUTS;
		var gfx_output_type = settings.gfx_output_type !== undefined ? settings.gfx_output_type : null;
		var settings_default_value = settings.settings_default_value !== undefined ? settings.settings_default_value : null;
		var processor = settings.processor;
		var image_path = settings.image_path;
		var is_editable = settings.is_editable;
		var nice_name = settings.nice_name;
		var width = settings.width;
		var height = settings.height;
		var labels = settings.labels;

		var on_editor_show = settings.on_editor_show;
		var on_each_frame = settings.on_each_frame;
		var on_gfx_init = settings.on_gfx_init;
		var on_created = settings.on_created;

		Node.registered_nodes[node_type] = function() {
			Node.registered_nodes.DefaultNode.call(this);

			this.num_outputs = num_outputs;
			this.num_inputs = num_inputs;
			this.gfx_output_type = gfx_output_type;
			this.processor = processor;
			this.settings_default_value = settings_default_value;
			this.node_type = node_type;
			this.is_editable = is_editable;
			this.image_path = image_path;
			this.nice_name = nice_name;
			this.width = width;
			this.height = height;
			this.labels = labels;

			this.on_editor_show = on_editor_show;
			this.on_each_frame = on_each_frame;
			this.on_gfx_init = on_gfx_init;
			this.on_created = on_created;
		};
	},

	make: function(settings) {
		if( settings.node_type === undefined || typeof settings.node_type != "string" ) {
			app_error.fatal('[Error when creating node] settings object expects a key "node_type" with a "string" type');
			return;
		}

		var node_type = settings.node_type;
		var variable_data = settings.variable_data;

		if( ! Node.registered_nodes[node_type] ) {
			app_error.fatal('Error: Trying to instanciate unknown or unregistered node ("'+node_type+'")');
			return;
		}

		var created_node = new Node.registered_nodes[node_type]();

		if( variable_data !== undefined && typeof variable_data === "object" ) {
			created_node.variable_data = variable_data;
		} else {
			created_node.variable_data = {};
		}

		if( created_node.settings_default_value && typeof created_node.settings_default_value === "object" ) {
			var node_settings_out_values = created_node.settings_default_value;

			for(var setting_key in node_settings_out_values) {
				if( created_node.variable_data[setting_key] === undefined ) {
					created_node.variable_data[setting_key] = node_settings_out_values[setting_key];
				}
			}
		}

		if( created_node.on_created ) {
			created_node.on_created();
		}

		Node.num_instanciated_nodes += 1;
		created_node.node_type = node_type;

		if( settings.node_id ) {
			created_node.node_identifier = settings.node_id;
		} else {
			created_node.node_identifier = node_type + Node.num_instanciated_nodes;
		}

		return created_node;
	},

	connect: function(settings) {
		/* Check mandatory parameters */
		if( settings.from === undefined || settings.to === undefined ) {
			app_error.warn('[Error when trying to connect nodes] Missing "from" or "to" key in Node.connect() call');
			return false;
		}

		for(var connect_endpoint in {from: true, to: true}) {
			if( settings[connect_endpoint].node && (! settings[connect_endpoint].node.is_node_object) ) {
				app_error.warn('[Error when trying to connect nodes] "'+connect_endpoint+'.node" must be a valid and instanciated Node');
				return false;
			}

			if( connect_endpoint == "from" ) {
				if( settings[connect_endpoint].output === undefined || typeof settings[connect_endpoint].output != "number" ) {
					app_error.warn('[Error when trying to connect nodes] "'+connect_endpoint+'.output" must be a valid node output index (Should be "O" if node has only one output)');
					return false;
				}
			} else {
				if( settings[connect_endpoint].input === undefined || typeof settings[connect_endpoint].input != "number" ) {
					app_error.warn('[Error when trying to connect nodes] "'+connect_endpoint+'.input" must be a valid node input index (Should be "O" if node has only one input)');
					return false;
				}
			}			
		}
		
		var node_A = settings.from.node;
		var node_B = settings.to.node;

		var node_A_output = settings.from.output;
		var node_B_input = settings.to.input;

		/* Check selected "To" Node's input is not already connected */
		if( node_B.inputs[node_B_input] ) {
			app_error.warn('[Error when trying to connect nodes] "to" node\'s selected input is already in use.');
			return false;
		}

		node_B.inputs[node_B_input] = node_A;

		return true;
	},

	update_connection_line_on_position_change: function(connector_line_def, connector_label_def, connection_type) {
		var node_A = connector_line_def.node_A;
    	var node_B = connector_line_def.node_B;
    	var node_A_output = connector_line_def.node_A_output;
    	var node_B_input = connector_line_def.node_B_input;

    	var from_position = node_A.gfx_get_output_connector_position(node_A_output, true);
    	var to_position = node_B.gfx_get_input_connector_position(node_B_input, true);

    	var nc_from_position = node_A.gfx_get_output_connector_position(node_A_output);
    	var nc_to_position = node_B.gfx_get_input_connector_position(node_B_input);

		from_position = {
			x: from_position.x + node_A.gfx.x,
			y: from_position.y + node_A.gfx.y,
		}

		to_position = {
			x: to_position.x + node_B.gfx.x,
			y: to_position.y + node_B.gfx.y,
		}
		connector_line_def.gfx.clear();

    	connector_line_def.gfx.beginFill(0xffffff, 0)
				.lineStyle(2, 0xffffff, 1)
				.moveTo(from_position.x, from_position.y)
		        .lineTo(to_position.x, to_position.y)
		        .endFill();
		
		switch( connection_type ) {
			case "input":
				var vertical_side = to_position.y > from_position.y ? 1 : -1;
				var label_x = - connector_label_def.width - NODE_CONNECTORS_LABEL_OFFSET;
				var label_y = (nc_to_position.y + NODE_CONNECTORS_SIZE * 0.5) - connector_label_def.height * 0.5 + vertical_side * 10;
				break;
			case "output":
				var vertical_side = to_position.y < from_position.y ? 1 : -1;
				var label_x = nc_from_position.x + NODE_CONNECTORS_LABEL_OFFSET;
				var label_y = (nc_from_position.y + NODE_CONNECTORS_SIZE * 0.5) - connector_label_def.height * 0.5 + vertical_side * 10;
				break;
			default: 
				break;
		}

		connector_label_def.position.set(label_x, label_y);
	}
}



/*********************************/
/*********************************/
/**       DEFAULT NODES         **/
/*********************************/
/*********************************/



/********************************/
/*         INPUT NODES          */
/********************************/

Node.register('ConstNode', {
	num_inputs: 0, 
	gfx_output_type: "settings",
	nice_name: 'Const input',
	settings_default_value: {
		"constant_value": 1,
	},
	processor: function() {
		this.value = this.variable_data.constant_value;
	},
	on_editor_show: function(save_callback) {
		var converted_value = convert_to_output_mode(this.variable_data.constant_value);

		var user_value = window.prompt(
			"Input new constant value (as "+Circuit.settings.output_mode+") :",
			converted_value
		);
		
		save_callback(user_value, 'constant_value');
	},
});

Node.register('ClockNode', {
	num_inputs: 0,
	image_path: 'img/node_clock.png',
	nice_name: 'Clock',
	gfx_output_type: "settings",

	settings_default_value: {
		"frequency": 1
	},
	processor: function(timer) {
		var clock_signal = 0.0;

		clock_signal = timer * this.variable_data.frequency;
		
		this.value = clock_signal;
	},
	on_editor_show: function(save_callback) {
		var converted_value = convert_to_output_mode(this.variable_data.frequency);

		var user_value = window.prompt(
			"Input new Clock frequency (as "+Circuit.settings.output_mode+", in Hz) :",
			converted_value
		);
		
		save_callback(user_value, 'frequency');
	},
});


/****************************************/
/*      OUTPUT/VISUALISATION NODES      */
/****************************************/

Node.register('OutputNode', {
	num_inputs: 1, 
	nice_name: 'Text Output',
	gfx_output_type: "text",
	processor: function() {
		this.value = this.inputs[0].value;
	},
	on_each_frame: function() {
		this.update_text_output();
	}
});

Node.register('XYOutputNode', {
	/* this.inputs[0] => X value */
	/* this.inputs[1] => Y value */
	num_inputs: 2, 
	num_outputs: 0,
	labels: {
		in: ['X', 'Y']
	},
	settings_default_value: {
		"keep_frames_number": 1,
		"dot_size": 1
	},
	nice_name: 'XY Output',
	gfx_output_type: "plane_2d",
	width: 300,
	height: 300,

	on_created: function() {
		this.dots_gfx = [];
		this.current_dot_index = 0;
	},

	on_gfx_init: function(gfx_width, gfx_height) {
		var mid_width = gfx_width * 0.5;
		var mid_height = gfx_height * 0.5;

		this.mid_width = mid_width;
		this.mid_height = mid_height;

		this.gfx_output_plane = new PIXI.Graphics();
		
		this.gfx_plane_2d_axis_x = new PIXI.Graphics();
		this.gfx_plane_2d_axis_x.lineStyle(1, NODE_GFX_AXIS_COLOR, 1);
		this.gfx_plane_2d_axis_x.moveTo(0, mid_height);
		this.gfx_plane_2d_axis_x.lineTo(gfx_width, mid_height);
		
		this.gfx_plane_2d_axis_y = new PIXI.Graphics();
		this.gfx_plane_2d_axis_y.lineStyle(1, NODE_GFX_AXIS_COLOR, 1);
		this.gfx_plane_2d_axis_y.moveTo(mid_width, 0);
		this.gfx_plane_2d_axis_y.lineTo(mid_width, gfx_height);

		this.gfx_output_plane.addChild(this.gfx_plane_2d_axis_x);
		this.gfx_output_plane.addChild(this.gfx_plane_2d_axis_y);

		return this.gfx_output_plane;
	},

	on_each_frame: function() {
		var x = this.inputs[0].value;
		var y = this.inputs[1].value;

		if( this.current_dot_index >= this.variable_data.keep_frames_number ) {
			this.current_dot_index = 0;
		}

		var dot_gfx = this.dots_gfx[this.current_dot_index];

		if( ! dot_gfx ) {
			dot_gfx = new PIXI.Graphics();

			dot_gfx.beginFill(NODE_GFX_POINT_COLOR, 1);
			dot_gfx.drawCircle(this.mid_width, this.mid_height, this.variable_data.dot_size);
			dot_gfx.endFill();

			this.dots_gfx[this.current_dot_index] = dot_gfx;
			this.gfx_output_plane.addChild(dot_gfx);
		}

		dot_gfx.position.set(x, -y);

		this.current_dot_index ++;
	},
});




/********************************/
/*       SIGNAL MOD NODES       */
/********************************/

Node.register('GainNode', {
	num_inputs: 1, 
	nice_name: 'Gain',
	gfx_output_type: "settings",
	settings_default_value: {
		"gain": 1,
	},
	processor: function() {
		this.value = this.inputs[0].value * this.variable_data.gain;
	},
	on_editor_show: function(save_callback) {
		var converted_value = convert_to_output_mode(this.variable_data.gain);

		var user_value = app_modal.prompt(
			"Input new Gain value (as "+Circuit.settings.output_mode+") :",
			converted_value
		);
		
		save_callback(user_value, 'gain');
	},
});

Node.register('OffsetNode', {
	num_inputs: 1, 
	nice_name: 'Offset',
	gfx_output_type: "settings",
	settings_default_value: {
		"offset": 1,
	},
	processor: function() {
		this.value = this.inputs[0].value + this.variable_data.offset;
	},
	on_editor_show: function(save_callback) {
		var converted_value = convert_to_output_mode(this.variable_data.offset);

		var user_value = app_modal.prompt(
			"Input new Offset value (as "+Circuit.settings.output_mode+") :",
			converted_value
		);
		
		save_callback(user_value, 'offset');
	},
});



/********************************/
/*      BASIC MATHS NODES       */
/********************************/

Node.register('MultiplicationNode', {
	num_inputs: 2, 
	image_path: 'img/node_multiplication.png',
	nice_name: 'Multiplication',
	processor: function() {
		this.value = this.inputs[0].value * this.inputs[1].value;
	}
});


Node.register('AdditionNode', {
	num_inputs: 2, 
	image_path: 'img/node_addition.png',
	nice_name: 'Addition',
	processor: function() {
		this.value = this.inputs[0].value + this.inputs[1].value;
	}
});




/********************************/
/*       TRIG FUNCS NODES       */
/********************************/

Node.register('SinNode', {
	num_inputs: 3,
	image_path: 'img/node_trig.png',
	nice_name: 'Sinus',
	labels: {
		in: ['in', 'amp', 'freq']
	},
	settings_default_value: {
		"amplitude": 100,
		"frequency": 1,
	},
	processor: function() {
		if( this.inputs[1] ) {
			this.variable_data.amplitude = this.inputs[1].value;
		}

		if( this.inputs[2] ) {
			this.variable_data.frequency = this.inputs[2].value;
		}

		var w = 2 * Math.PI * this.variable_data.frequency;
		this.value = this.variable_data.amplitude * Math.sin(w * this.inputs[0].value);
	}
});

Node.register('CosNode', {
	num_inputs: 3,
	image_path: 'img/node_trig.png',
	nice_name: 'Cosinus',
	labels: {
		in: ['in', 'amp', 'freq']
	},
	settings_default_value: {
		"amplitude": 100,
		"frequency": 1,
	},
	processor: function() {
		if( this.inputs[1] ) {
			this.variable_data.amplitude = this.inputs[1].value;
		}

		if( this.inputs[2] ) {
			this.variable_data.frequency = this.inputs[2].value;
		}

		var w = 2 * Math.PI * this.variable_data.frequency;
		this.value = this.variable_data.amplitude * Math.cos(w * this.inputs[0].value);
	}
});



/********************************/
/*   BITWISE OPERATORS NODES    */
/********************************/

Node.register('AndNode', {
	num_inputs: 2,
	image_path: 'img/node_and.png',
	nice_name: 'And',
	processor: function() {
		this.value = this.inputs[0].value & this.inputs[1].value;
	}
});

Node.register('OrNode', {
	num_inputs: 2, 
	image_path: 'img/node_or.png',
	nice_name: 'Or',
	processor: function() {
		this.value = this.inputs[0].value | this.inputs[1].value;
	}
});

Node.register('XorNode', {
	num_inputs: 2, 
	image_path: 'img/node_xor.png',
	nice_name: 'Xor',
	processor: function() {
		this.value = this.inputs[0].value ^ this.inputs[1].value;
	}
});

Node.register('NotNode', {
	num_inputs: 1, 
	image_path: 'img/node_not.png',
	nice_name: 'Not',
	processor: function() {
		var bin_value = this.inputs[0].get_text_output_string('binary');
		var not_value = "";
		for(var bit_index in bin_value) {
			not_value += parseInt(bin_value[bit_index]) ? 0 : 1;
		}
		this.value = parseInt(not_value, 2);
	}
});



