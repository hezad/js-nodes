/**
 * Application main module
 *
 * Default GUI menu entries are registered at the end of this file
 */



/*********************************/
/*********************************/
/**       APP MAIN MODULE       **/
/*********************************/
/*********************************/

var APP_LOCAL_STORAGE_PREFIX = 'jsnode_circuit';

var App = {

	/* App version for saving/loading content compatibility check */
	FILE_VERSION: 7,

	/* dat.gui menu instance */
	dat_gui: null,
	registered_menu: {},
	__instanciated_registered_menu: null,

	/* Settings */
	defaults: {},
	settings: {},

	is_new_circuit: true,

	/* Methods */
	init: function(options) {
		if( ! options ) { options = {}; }
		App.settings = $.extend(App.defaults, options);

		App.dat_gui = new dat.GUI();

		App.init_circuit();
		App.init_menu();
		App.update_menu();

		on_after_window_resize(function(e) {
			Circuit.update_stage_size();
		});
	},

	register_menu: function(menu_definer) {
		App.registered_menu = menu_definer;
	},

	init_menu: function(menu_data, gui_wrapper) {
		if( ! menu_data ) {
			App.__instanciated_registered_menu = App.registered_menu();
			menu_data = App.__instanciated_registered_menu;
			gui_wrapper = App.dat_gui;
		}

		for(var entry_title in menu_data) {
			var menu_entry = menu_data[entry_title];

			/* Entry is a folder with sub entries */
			if( menu_entry.folder ) {
				var gui_folder = gui_wrapper.addFolder(entry_title);
				App.init_menu(menu_entry.folder, gui_folder);
				if( menu_entry.auto_open ) {
					gui_folder.open();
				}
			} else {
				if( ! menu_entry.onChange ) {
					menu_entry.onChange = function(value) { return; };
				}

				if( menu_entry.options ) {
					var gui_controller = gui_wrapper.add(menu_entry.object, menu_entry.key, menu_entry.options).onChange(menu_entry.onChange);
				} else {
					var gui_controller = gui_wrapper.add(menu_entry.object, menu_entry.key).onChange(menu_entry.onChange);
				}

				if( menu_entry.disabled === true ) {
					window.__gui_controller = gui_controller;
					$(gui_controller.__li).addClass('disabled');
					$(gui_controller.__li).children().children('.c').children('input,select,button').attr('disabled', true).addClass('disabled');
				}
				
				$(gui_controller.__li).children().children('span').text(entry_title);
			}
		}
	},

	update_menu: function(gui_wrapper) {
		if( ! gui_wrapper ) {
			gui_wrapper = App.dat_gui;
		}

		if( gui_wrapper.__folders && (! $.isEmptyObject(gui_wrapper.__folders)) ) {
			for (var i in gui_wrapper.__folders) {
				App.update_menu(gui_wrapper.__folders[i]);
			}
		}

		for (var i in gui_wrapper.__controllers) {
			gui_wrapper.__controllers[i].updateDisplay();
		}
	},

	init_circuit: function() {
		Circuit.init_gfx();
		Circuit.init_settings();
	},

	load_json: function(json_data) {
		Circuit.reset();
		Circuit.init_gfx();
		Circuit.from_json(json_data);
		Circuit.run();
	},

	import_circuit: function(json_data) {
		App.load_json(json_data);
	},

	menu_import_circuit: function() {
		load_json_file(function(json_data, file_data) {
			if( json_data.FILE_VERSION === undefined ) {
				app_modal.alert(file_data.name + ' is not a valid JSNodes file.\n\nCheck console for more info.');
				app_error.warn('FILE_VERSION not found in "'+file_data.name+'"');
				return;
			}

			if( json_data.FILE_VERSION !== App.FILE_VERSION ) {
				app_modal.alert(file_data.name + ' is incompatible with this version of JSNodes.\n\nCheck console for more info.');
				app_error.warn('App version: '+App.FILE_VERSION+"\n"+'File version: '+json_data.FILE_VERSION);
				return;
			}

			App.import_circuit(json_data);
		});
	},

	export_circuit: function() {
		var filename = string_to_nice_filename(Circuit.settings.circuit_name);
		var json_data = Circuit.to_json();
		download_file(json_data, filename, "application/json");
	},

	menu_export_circuit: function() {
		App.export_circuit();
	},

	save_circuit: function(force_circuit_name) {
		var circuit_name = Circuit.settings.circuit_name;

		if( force_circuit_name ) {
			circuit_name = force_circuit_name;
		}

		var storage_circuit_id = APP_LOCAL_STORAGE_PREFIX + "_" + string_to_nice_filename(circuit_name);
		var can_save_circuit = true; 

		if( localStorage.getItem(storage_circuit_id) ) {
			can_save_circuit = app_modal.confirm("There is already a circuit named '"+circuit_name+"'.\n\nDo you want to overwrite it?");
		}

		if( can_save_circuit ) {
			Circuit.settings.circuit_name = circuit_name;

			localStorage.setItem(storage_circuit_id, Circuit.to_json());
			App.is_new_circuit = false;

			app_modal.success("Circuit '"+Circuit.settings.circuit_name+"' saved.");
		}

		App.update_menu();
	},

	menu_save_circuit: function() {
	    if( App.is_new_circuit ) {
	    	App.menu_save_circuit_as();
	    } else {
	    	App.save_circuit();
	    }		
	},

	menu_save_circuit_as: function() {
		var circuit_name = app_modal.prompt('Choose a name for your circuit:', Circuit.settings.circuit_name);
		if( circuit_name === null ) {
			return;
		}

		App.save_circuit(circuit_name);
	},

	menu_load_circuit: function() {
		var storage_keys = Object.keys(localStorage);
		var jsnodes_saves = {};
		var loading_dialog_modal_entries = {};

		for(var i = 0; i < storage_keys.length; i++) {
			var storage_key_match = storage_keys[i].match(new RegExp('^'+APP_LOCAL_STORAGE_PREFIX+'_(.+)'));
			if( storage_key_match ) {
				var local_key = storage_key_match[1];
				var circuit_data = localStorage.getItem(storage_keys[i]);
				var json_data = null;

				try {
					json_data = JSON.parse(circuit_data);

					if( json_data.FILE_VERSION === undefined ) {
						app_error.warn("Can't load localStorage circuit "+local_key+": Invalid save format (FILE_VERSION not found)");
						continue;
					}

					if( json_data.FILE_VERSION !== App.FILE_VERSION ) {
						app_error.warn('localStorage Circuit "' + local_key + '" is incompatible with this version of JSNodes.');
						app_error.warn('	=> App version: '+App.FILE_VERSION+' / File version: '+json_data.FILE_VERSION);
						app_error.info('	=> Type \'localStorage.removeItem("jsnode_circuit_'+local_key+'")\' in the console to delete it.');
						continue;
					}

					jsnodes_saves[local_key] = json_data;
					loading_dialog_modal_entries[local_key] = jsnodes_saves[local_key].circuit_settings.circuit_name;
				} catch (ex) {
					app_error.warn("Can't load localStorage circuit "+local_key+".\n\nError when parsing JSON:\n" + ex + "\n\nAborting loading.");
					continue;
				}
			}
		}

		if( jsnodes_saves ) {
			( function(jsnodes_saves) {
				app_modal.list_select("Select Circuit to load", loading_dialog_modal_entries, function(circuit_id) {
					var json_data = jsnodes_saves[circuit_id];
					App.load_json(json_data);
					App.is_new_circuit = false;
				});
			}) (jsnodes_saves);
		} else {
			app_modal.alert("Can't find any Circuit to load !");
		}
	}
}



/*********************************/
/*********************************/
/**  APP DEFAULT MENU ENTRIES   **/
/*********************************/
/*********************************/




App.register_menu( function() {
	return {
		"Circuit name": {
			key: 'circuit_name',
			object: Circuit.settings,
			disabled: true,
			// onChange: function(value) {
			// 	Circuit.set_circuit_name(value);
			// }
		},
		"File": {
			auto_open: true,
			folder: {
				"Load": {
					key: "menu_load_circuit",
					object: App,
				},
				"Save": {
					key: "menu_save_circuit",
					object: App,
				},
				"Save As": {
					key: "menu_save_circuit_as",
					object: App,
				},
				"Import": {
					key: "menu_import_circuit",
					object: App
				},
				"Export": {
					key: "menu_export_circuit",
					object: App
				},
			}
		},
		"Settings": {
			auto_open: true,
			folder: {
				"Show i/o labels": {
					key: "show_nodes_in_out_labels",
					object: Circuit.settings,
					options: ["no", "yes"],
					onChange: function(value) {
						Circuit.set_show_nodes_in_out_labels_value(value);
					}
				},
				"Output mode": {
					key: "output_mode",
					object: Circuit.settings,
					options: Circuit.supported_output_modes,
					onChange: function(value) {
						Circuit.set_output_mode(value);
					}
				}
			}
		}
	};
});

