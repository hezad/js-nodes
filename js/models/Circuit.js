var CIRCUIT_BACKGROUND_COLOR = 0x888888;
var CIRCUIT_DEFAULT_NAME = "New circuit";
var PER_MILLISECONDS_MULTIPLIER = 1.0 / 1000;

var Circuit = {
	nodes: {},
	end_nodes: [],

	defaults: {
		circuit_name: CIRCUIT_DEFAULT_NAME,
		output_mode: "decimal",
		show_nodes_in_out_labels: "yes",
	},
	settings: {},

	gfx_dom_container: document.body,

	gfx_engine: null,
	gfx_connection_lines: {},

	supported_output_modes: Object.keys(Node.all_output_modes),

	is_running: false,
	should_find_end_nodes: false,
	is_processing_cycle_active: false,

	animation_frame_request_handler: null,

	connecting_interaction: {
		is_active: false,
		data: {
			from: null,
			to: null
		},
		temp_connection_line_gfx: null
	},

	/* Data/Identifiers processing */
	is_connection_valid: function(from_inout_type, from_connector_gfx, to_inout_type, to_connector_gfx) {
		var from_identifier = from_connector_gfx.parentNode.node_identifier + ":" + from_connector_gfx.node_connector_index;
		var to_identifier = to_connector_gfx.parentNode.node_identifier + ":" + to_connector_gfx.node_connector_index;

		// If same connector
		if( from_identifier == to_identifier ) {
			return false;
		}

		// If connecting two inputs or two outputs
		if( from_inout_type == to_inout_type ) {
			app_error.warn("Can't connect inputs together or outputs together");
			return false;
		}

		// If connection already exists
		if( from_inout_type == "output" ) {
			var connection_identifier = from_identifier + ">" + to_identifier;
		} else {
			var connection_identifier = to_identifier + ">" + from_identifier;
		}
		
		if( Circuit.gfx_connection_lines[connection_identifier] !== undefined ) {
			app_error.warn("Connection already exists");
			return false;
		}

		return true;
	},

	make_connection_identifier: function(from_node, from_node_output, to_node, to_node_input) {
		return from_node.node_identifier + ":" + from_node_output + ">" + to_node.node_identifier + ":" + to_node_input;
	},

	set_show_nodes_in_out_labels_value: function(show_value) {
		show_value = show_value.toLowerCase();

		Circuit.settings.show_nodes_in_out_labels = show_value;

		for(var ni in Circuit.nodes) {
			var labels = Circuit.nodes[ni].gfx_input_labels;
			for(var nj = 0; nj < labels.length; nj ++) {
				labels[nj].visible = Circuit.settings.show_nodes_in_out_labels === "yes" ? true: false;
			}
			var labels = Circuit.nodes[ni].gfx_output_labels;
			for(var nj = 0; nj < labels.length; nj ++) {
				labels[nj].visible = Circuit.settings.show_nodes_in_out_labels === "yes" ? true: false;
			}
		}
	},

	set_output_mode: function(output_mode) {
		output_mode = output_mode.toLowerCase();

		if( Circuit.supported_output_modes.indexOf(output_mode) == -1 ) {
			app_error.fatal('[Error when trying to set Circuit\'s output mode] "'+output_mode+'" is not a supported mode.');
			return;
		}
		
		Circuit.settings.output_mode = output_mode;
	},

	set_circuit_name: function(circuit_name) {
		Circuit.settings.circuit_name = circuit_name;
	},

	/* Nodes Editing Methods */
	add_node: function(node_type, position_data, variable_data, node_id) {
		var new_node = Node.make({
			node_type: node_type,
			variable_data: variable_data,
			node_id: node_id
		});

		new_node.gfx_init(Circuit.gfx_engine.stage, position_data.x, position_data.y);
		Circuit.nodes[new_node.node_identifier] = new_node;

		Circuit.should_find_end_nodes = true;

		return Circuit.nodes[new_node.node_identifier];
	},

	connect_nodes: function(settings) {
		if( ! Node.connect(settings) ) {
			return false;
		}

		var node_A = settings.from.node;
		var node_B = settings.to.node;

		var node_A_output = settings.from.output;
		var node_B_input = settings.to.input;

		var from_position = node_A.gfx_get_output_connector_position(true);
		var to_position = node_B.gfx_get_input_connector_position(node_B_input, true);

		from_position = {
			x: from_position.x + node_A.gfx.x,
			y: from_position.y + node_A.gfx.y,
		}

		to_position = {
			x: to_position.x + node_B.gfx.x,
			y: to_position.y + node_B.gfx.y,
		}

		var connection_id = Circuit.make_connection_identifier(node_A, node_A_output, node_B, node_B_input);
		var line_gfx = new PIXI.Graphics();
		line_gfx.beginFill(0xffffff, 0)
				.lineStyle(2, 0xffffff, 1)
				.moveTo(from_position.x, from_position.y)
		        .lineTo(to_position.x, to_position.y)
		        .endFill();
		
		Circuit.gfx_connection_lines[connection_id] = line_gfx;

		var connection_lines_def = {node_B: node_B, node_A: node_A, gfx: line_gfx, node_A_output: node_A_output, node_B_input: node_B_input};
		node_B.gfx_input_connection_lines[node_B_input] = connection_lines_def;
		node_A.gfx_output_connection_lines[node_A_output] = connection_lines_def;

		Circuit.gfx_engine.stage.addChild(Circuit.gfx_connection_lines[connection_id]);

		return true;
	},

	find_end_nodes: function() {
		for(var ni in Circuit.nodes) {
			if( Object.keys(Circuit.nodes[ni].gfx_output_connection_lines).length == 0 ) {
				Circuit.end_nodes.push(Circuit.nodes[ni]);
			}
		}
		Circuit.should_find_end_nodes = false;
	},

	/* Reseters / Initiators */
	reset: function() {
		Circuit.stop();
		Circuit.gfx_destroy();

		Node.num_instanciated_nodes = 0;

		Circuit.nodes = {};
		Circuit.end_nodes = [];
		Circuit.settings = $.extend(Circuit.defaults, {});
	},

	update_stage_size: function() {
		var $window = $(window);

		var width = $window.width() - ($(App.dat_gui.domElement).width() + 3);
		var height = $window.height();

		Circuit.gfx_engine.renderer.view.width = width;
		Circuit.gfx_engine.renderer.view.height = height;
		Circuit.gfx_engine.renderer.resize(width, height);
	},

	init_gfx: function() {
		Circuit.gfx_engine = {
			renderer: PIXI.autoDetectRenderer(
				1, 
				1,
				{
					antialias: true,
					transparent: false, 
					resolution: 1
				}
			),
			stage: new PIXI.Container()
		}

		Circuit.update_stage_size();

		Circuit.gfx_engine.renderer.backgroundColor = CIRCUIT_BACKGROUND_COLOR;
		Circuit.gfx_engine.renderer.view.style.position = "absolute";
		Circuit.gfx_dom_container.appendChild(Circuit.gfx_engine.renderer.view);
		Circuit.gfx_engine.renderer.clear();
	},

	init_settings: function(options) {
		Circuit.settings = $.extend(Circuit.defaults, options);
	},

	/* GFX Methods */
	gfx_destroy: function() {
		Circuit.gfx_engine.stage.destroy(true);
		Circuit.gfx_engine.stage = null;

		Circuit.gfx_dom_container.removeChild(Circuit.gfx_engine.renderer.view);
		Circuit.gfx_engine.renderer.destroy( true );
		Circuit.gfx_engine.renderer = null;
	},

	gfx_render_frame: function() {
		for(var ni in Circuit.nodes) {
			Circuit.nodes[ni].gfx_update_frame();
		}
		Circuit.gfx_engine.renderer.clear();
		Circuit.gfx_engine.renderer.render(Circuit.gfx_engine.stage);
	},

	/* Processing methods */
	process_nodes: function(timer) {
		for(var i = 0; i < Circuit.end_nodes.length; i++) {
			var end_node = Circuit.end_nodes[i];

			end_node.process(timer);
		}
	},

	pause_processing: function() {
		Circuit.is_running = false;
	},

	resume_processing: function() {
		Circuit.is_running = true;
	},

	run: function() {
		App.update_menu();

		Circuit.is_running = true;
		Circuit.is_processing_cycle_active = true;

		/* Waiting for sprites to be loaded (@TODO: automatize preloading instead of arbitrary delay) */
		setTimeout( function() {

			var initTime = (new Date()).getTime();
			var timer = 0;

			Circuit.animation_frame_request_handler = requestAnimationFrame(circuit_cycle);

			function circuit_cycle() {
				var currentTime = (new Date()).getTime();
				timer = (currentTime - initTime) * PER_MILLISECONDS_MULTIPLIER;

				if( Circuit.is_processing_cycle_active ) {
					Circuit.animation_frame_request_handler = requestAnimationFrame(circuit_cycle);

					if( Circuit.should_find_end_nodes ) {
						Circuit.find_end_nodes();
					}

					if( Circuit.is_running ) {
					    Circuit.process_nodes(timer);
					    Circuit.gfx_render_frame();
					}
				}
			}
		}, 1000);
	},

	stop: function() {
		Circuit.is_running = false;
		Circuit.is_processing_cycle_active = false;
	},

	/* Saving/loading methods */
	to_json: function() {
		var json_nodes = {};

		for(var node_id in Circuit.nodes) {
			var source_node = Circuit.nodes[node_id];

			json_nodes[node_id] = {
				node_type: source_node.node_type,
				node_identifier: node_id,
				variable_data: source_node.variable_data,
				position: { 
					x: source_node.gfx.position.x, 
					y: source_node.gfx.position.y 
				}
			};
		}

		var data_object = {
			FILE_VERSION: App.FILE_VERSION,
			circuit_settings: Circuit.settings,
			num_instanciated_nodes: Node.num_instanciated_nodes,
			nodes: json_nodes,
			connections: Object.keys(Circuit.gfx_connection_lines)
		};
		
		return JSON.stringify(data_object);
	},

	from_json: function(json_data) {
		if( Circuit.is_processing_cycle_active || Circuit.is_running ) {
			var msg = "Circuit must be stopped before loading a JSNodes file.";
			app_modal.alert(msg);
			app_error.warn(msg);
			return;
		}
		
		Circuit.init_settings(json_data.circuit_settings);

		var loaded_nodes = {};

		for(var node_id in json_data.nodes) {
			var node_data = json_data.nodes[node_id];
			loaded_nodes[node_id] = Circuit.add_node(node_data.node_type, node_data.position, node_data.variable_data, node_id);
		}

		for(var i = 0; i < json_data.connections.length; i++) {
			var connection_string = json_data.connections[i];
			var connection_bounds_strings = connection_string.split('>');

			var from_node_id_and_output = connection_bounds_strings[0].split(':');
			var to_node_id_and_input = connection_bounds_strings[1].split(':');

			var from_node = loaded_nodes[from_node_id_and_output[0]];
			var from_output = parseInt(from_node_id_and_output[1]);

			var to_node = loaded_nodes[to_node_id_and_input[0]];
			var to_input = parseInt(to_node_id_and_input[1]);

			Circuit.connect_nodes({
				from: { node: from_node, output: from_output },
				to: { node: to_node, input: to_input }
			});
		}
	}
}